

**How I became a designer day 3**
---------------------------------

De tweede studiodag begon met een hoop vraagtekens. Met betrekking tot de opleiding en de projectstof. Voor mij was het nog niet helemaal duidelijk wat er nou precies van ons verwacht wordt in het project en wat de deliverables precies zouden moeten inhouden of hoe deze eruit zouden moeten zien.

De studiodag begon dan ook met een korte introductie en de mogelijkheid tot het stellen van vragen. Aan de hand van deze vragen en antwoorden heb ik nu een aardig beeld van de verwachtingen en het idee dat we als groep momenteel goed op weg zijn in de richting van ons eerste oplever moment volgende week vrijdag. 

Vandaag heb ik voor het eerst meer kennis dan alleen het opstarten gemaakt met Adobe InDesign. Gelukkig voor mij zitten er in mijn project groep niet één maar drie mensen die alle hoeken van dit programma kennen. Zo heb ik met wat hulp binnen een half uur de eerste basiskennis tot me kunnen nemen en het direct in de praktijk kunnen brengen in het omzetten van mijn persoonlijke onderzoek naar het openbaar vervoer en het verkeer in het algemeen binnen Rotterdam van een saai pages document naar een visueel enigszins strak (je moet toch kritisch blijven en niet snel tevreden zijn) bestand om te zetten. 

De dag vervolgde zich met een introductie van onze studiecoach. Na 3 dagen les was hier dan eindelijk het moment en mogelijkheid om kennis te maken met ons klas en de klas groter te maken dan alleen onze eigen projectgroep. Dit maakt uiteraard de communicatie in het algemeen wat makkelijker onderling en versneld ook ons groepsproces. Verder was er tijdens deze eerste kennismaking een hoop ruimte voor ons om de nodige vragen beantwoord te krijgen met betrekking tot de opleiding.

Het middaggedeelte was voor een groot deel voorbestemd voor een workshop met betrekking tot het schrijven en bijhouden van een blog. Dit blog dient om later samen te voegen tot een leerdossier. Ik heb besloten om mijn blog How I became a designer te noemen. Persoonlijk vond ik de naam een leuke vergelijking tot de tv serie en passend binnen het concept van een blog/dagboek over dit onderwerp. Ik zit nog wel in een kleine twijfel met betrekking tot het publiceren van het blog. Persoonlijk wil ik heel graag mijn kennis met betrekking tot HTML en CSS ophalen en ergens is dit de perfecte mogelijkheid en ligt er dus de mogelijkheid om dit blog niet via een standaard programma te gaan bijhouden maar vanuit codering. Verrasend genoeg bleek er toch nog wat kennis te zijn blijven hangen met betrekking tot deze HTML codering van twee jaar informatica op de HAVO. 

De rest van de middag heb ik besteed aan het afronden van mijn persoonlijke onderzoek. Binnen de groep hebben we afgesproken om morgen onze onderzoeken naast elkaar te leggen zodat we morgen aan het eind van de dag een definitieve keuze kunnen maken met betrekking tot ons onderzoek en het verder aan te houden thema tijdens ons project. 


